﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uzdevums2.Dto
{
    public class BalanceDto
    {
        public decimal Balance { get; set; }
        public decimal Debt { get; set; }
        public decimal Loaned { get; set; }
    }
}
