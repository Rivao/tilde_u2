﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Uzdevums2.Common;
using Uzdevums2.Dto;
using Uzdevums2.Models;

namespace Uzdevums2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoansController : ControllerBase
    {
        private readonly uzdevums2Context _context;

        public LoansController(uzdevums2Context context)
        {
            _context = context;
        }

        // GET: api/Loans
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Loan>>> GetLoans()
        {
            return await _context.Loans.ToListAsync();
        }

        // GET: api/Loans/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Loan>> GetLoan(int id)
        {
            var loan = await _context.Loans.FindAsync(id);

            if (loan == null)
            {
                return NotFound();
            }

            return loan;
        }

        // PUT: api/Loans/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoan(int id, Loan loan)
        {
            if (id != loan.Id)
            {
                return BadRequest();
            }

            _context.Entry(loan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Loans
        [HttpPost]
        public async Task<ActionResult<Loan>> PostLoan(Loan loan)
        {
            _context.Loans.Add(loan);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoan", new { id = loan.Id }, loan);
        }

        // DELETE: api/Loans/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Loan>> DeleteLoan(int id)
        {
            var loan = await _context.Loans.FindAsync(id);
            if (loan == null)
            {
                return NotFound();
            }

            _context.Loans.Remove(loan);
            await _context.SaveChangesAsync();

            return loan;
        }
        // GET: api/Loans/getBalance/5
        [HttpGet("getBalance/{id}")]
        public BalanceDto GetBalance(int id)
        {
            var balance = new BalanceDto();
            var loanTaken = _context.Loans.Where(l => l.LoaneeId == id
                && l.Status.ToLower() == Enums.LoanStatus.Active.ToString().ToLower()).ToList();
            var loanGiven = _context.Loans.Where(l => l.LoanerId == id
                && l.Status.ToLower() == Enums.LoanStatus.Active.ToString().ToLower()).ToList();
            // theoretically, this is business logic and should be created as a service
            // that would be injected into the controller
            if (loanTaken.Count > 0)
            {
                decimal sum = 0;
                foreach (var loan in loanTaken)
                {
                    sum += loan.Amount;
                }
                balance.Debt = sum;
            }
            else
            {
                balance.Debt = 0;
            }

            if (loanGiven.Count > 0)
            {
                decimal sum = 0;
                foreach (var loan in loanGiven)
                {
                    sum += loan.Amount;
                }
                balance.Loaned = sum;
            }
            else
            {
                balance.Loaned = 0;
            }
            balance.Balance = balance.Loaned - balance.Debt;
            return balance;
        }
        // GET: api/Loans/getPersonsLoans/5
        [HttpGet("getPersonsLoans/{id}")]
        public List<Loan> GetPersonsLoans(int id)
        {
            var loan = _context.Loans.Where(l => (l.LoaneeId == id || l.LoanerId == id) 
                && l.Status.ToLower() == Enums.LoanStatus.Active.ToString().ToLower()).ToList();
            return loan;
        }

        private bool LoanExists(int id)
        {
            return _context.Loans.Any(e => e.Id == id);
        }
    }
}
