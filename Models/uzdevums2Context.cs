﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Uzdevums2.Models
{
    public partial class uzdevums2Context : DbContext
    {
        public uzdevums2Context()
        {
        }

        public uzdevums2Context(DbContextOptions<uzdevums2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Loan> Loans { get; set; }
        public virtual DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=uzdevums2;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Loan>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.LoaneeId).HasColumnName("loaneeId");

                entity.Property(e => e.LoanerId).HasColumnName("loanerId");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Loanee)
                    .WithMany(p => p.LoanLoanee)
                    .HasForeignKey(d => d.LoaneeId)
                    .HasConstraintName("FK__Loan__loaneeId__276EDEB3");

                entity.HasOne(d => d.Loaner)
                    .WithMany(p => p.LoanLoaner)
                    .HasForeignKey(d => d.LoanerId)
                    .HasConstraintName("FK__Loan__loanerId__267ABA7A");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
