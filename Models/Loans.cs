﻿using System;
using System.Collections.Generic;

namespace Uzdevums2.Models
{
    public partial class Loan
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public int LoanerId { get; set; }
        public int LoaneeId { get; set; }
        public decimal Amount { get; set; }

        public virtual Person Loanee { get; set; }
        public virtual Person Loaner { get; set; }
    }
}
