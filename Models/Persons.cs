﻿using System;
using System.Collections.Generic;

namespace Uzdevums2.Models
{
    public partial class Person
    {
        public Person()
        {
            LoanLoanee = new HashSet<Loan>();
            LoanLoaner = new HashSet<Loan>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Loan> LoanLoanee { get; set; }
        public virtual ICollection<Loan> LoanLoaner { get; set; }
    }
}
