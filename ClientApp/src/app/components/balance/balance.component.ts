import { Component, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BalanceDto } from 'src/app/common/dtos';
import { LoanStatus } from 'src/app/common/enums';
import { ApiService } from 'src/app/services/api-service';
import { StateService } from 'src/app/services/state-service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent {
  balance: BalanceDto;
  personId: number;
  loans: Loan[];

  constructor(
    private _api: ApiService,
    private _state: StateService,
    public route: ActivatedRoute,
    private _ngZone: NgZone
    ) {
        this.personId = +this.route.snapshot.paramMap.get('id');
    }

  async ngOnInit() {
    if (this._state.persons.length === 0) {
        await this._api.getPersons().then(result => {
            this._state.persons = result;
        })
    }
    this._api.getPersonsLoans(this.personId).then(loans => {
        this.loans = loans;
    })
    this._api.getBalance(this.personId).then(result => {
        this.balance = result;
    })
  }

  getPersonName(id: number): string {
    let person = this._state.persons.find(p => p.id === id);
    return person.firstName + ' ' + person.lastName;
  }

  async markLoanPaid(id: number) {
      let index = this.loans.findIndex(l => l.id == id);
      this.loans[index].status = LoanStatus.Paid;
      await this._api.editLoan(this.loans[index]);
      this.loans.splice(index, 1);
      var result = await this._api.getBalance(this.personId);
      // by defaultasync operations finish outside of angular zone, so UI does not register model changes
      this._ngZone.run(() => {
        this.balance = result;
      });
  }
}
