import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api-service';
import { StateService } from 'src/app/services/state-service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
})
export class PersonFormComponent {
    constructor(
        private _api: ApiService,
        private _state: StateService,
        private _location: Location) {}

    personForm = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
    });

    async onSubmit() {
        let person: Person = { 
            firstName: this.personForm.controls['firstName'].value,
            lastName: this.personForm.controls['lastName'].value     
        }
        let result = await this._api.postPerson(person);
        this._state.addPerson(result);
        this._location.back();
    }
}
