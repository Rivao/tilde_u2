import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api-service';
import { StateService } from 'src/app/services/state-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  
  public persons: Person[];

  constructor(
    api: ApiService,
    state: StateService
    ) {
      // make an api call only on first initialization
      if (state.persons.length == 0) {
        api.getPersons().then(result => {
          this.persons = result;
          state.persons = result;
        });
      } else {
        this.persons = state.persons;
      }
  }
}
