import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api-service';
import { StateService } from 'src/app/services/state-service';
import { LoanStatus } from 'src/app/common/enums';

@Component({
  selector: 'app-loan-form',
  templateUrl: './loan-form.component.html',
})
export class LoanFormComponent {
    baseUrl: string;
    persons: Person[];
    constructor( 
        private _state: StateService, 
        private _api: ApiService,
        private _location: Location ) {}
    loanForm = new FormGroup({
        loaner: new FormControl(null, Validators.required),
        loanee: new FormControl(null, Validators.required),
        amount: new FormControl('', Validators.required),
    }, this.samePersonsSelectedValidator);
    
    samePersonsSelectedValidator(control: AbstractControl) {
        let loaner = control.get('loaner').value;
        let loanee = control.get('loanee').value;
        return loaner === loanee ? {samePerson: true} : null;
    }
    ngOnInit() {
        this.persons = this._state.persons;
        // page has been refreshed, so we need to fetch data anew and set default values
        if (this.persons.length == 0) {
            this._api.getPersons().then(result => {
                this.persons = result;
                this._state.persons = result;
                this.loanForm.patchValue({
                    loaner: this.persons[0].id,
                    loanee: this.persons[1].id
                });
            });
        } else {
            this.loanForm.patchValue({
                loaner: this.persons[0].id,
                loanee: this.persons[1].id
            });
        }
    }
    async onSubmit() {
        let loanerId = this.loanForm.controls['loaner'].value;
        let loaneeId = this.loanForm.controls['loanee'].value;
        let loan: Loan = { 
            loanerId: loanerId,
            loaneeId: loaneeId,
            amount: this.loanForm.controls['amount'].value,
            status: LoanStatus.Active
        }
        await this._api.postLoan(loan);
        this._location.back();
    }
}
