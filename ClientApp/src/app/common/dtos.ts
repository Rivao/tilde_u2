export class BalanceDto {
    balance: number;
    debt: number;
    loaned: number;
}