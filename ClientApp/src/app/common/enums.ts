export enum LoanStatus {
    Active = "ACTIVE",
    Paid = "PAID"
}