interface Loan {
    id?: number,
    status: string,
    loanerId: number,
    loaneeId: number,
    amount: number
}