import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
// We use single class for simple state management
// in a real app, we would use separate service classes for each model
export class StateService {
    private _persons = new BehaviorSubject<Person[]>([]);

    readonly persons$ = this._persons.asObservable();

    get persons(): Person[] {
        return this._persons.getValue();
    }

    set persons(val: Person[]) {
        this._persons.next(val);
    }

    addPerson(person: Person) {
        this.persons = [
          ...this.persons, 
          person
        ];
    }
}