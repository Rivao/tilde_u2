import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { BalanceDto } from "../common/dtos";

@Injectable()
export class ApiService {
    baseUrl = environment['baseURL'];
    constructor(private http: HttpClient) {}

    async getPersons(): Promise<Person[]> {
        let result = await this.http.get<Person[]>(this.baseUrl + 'persons').toPromise();
        return result;
    }

    async postPerson(person: Person): Promise<Person> {
        let result = await this.http.post<Person>(this.baseUrl + 'persons', person).toPromise();
        return result;
    }

    async getLoans(): Promise<Loan[]> {
        let result = await this.http.get<Loan[]>(this.baseUrl + 'loans').toPromise();
        return result;
    }

    async editLoan(loan: Loan): Promise<void> {
        await this.http.put<Loan>(this.baseUrl + `loans/${loan.id}`, loan).toPromise();
    }

    async getPersonsLoans(id: number): Promise<Loan[]> {
        let result = await this.http.get<Loan[]>(this.baseUrl + `loans/getPersonsLoans/${id}`).toPromise();
        return result;
    }

    async postLoan(loan: Loan): Promise<Loan> {
        let result = await this.http.post<Loan>(this.baseUrl + 'loans', loan).toPromise();
        return result;
    }

    async getBalance(id: number) {
        let result = await this.http.get<BalanceDto>(this.baseUrl + `loans/getBalance/${id}`).toPromise();
        return result;
    }
}
